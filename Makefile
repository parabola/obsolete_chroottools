V=20121128.6

PREFIX = /usr/local
pkgdatadir=$(PREFIX)/share/devtools

BINPROGS = \
	checkpkg \
	find-libdeps \
	finddeps \
	lddd

SBINPROGS = \
	mkarchroot

all: $(BINPROGS) $(SBINPROGS) bash_completion zsh_completion

edit = sed -e "s|@pkgdatadir[@]|$(pkgdatadir)|g"

%: %.in Makefile
	@echo "GEN $@"
	@$(RM) "$@"
	@m4 -P $@.in | $(edit) >$@
	@chmod a-w "$@"
	@chmod +x "$@"

clean:
	rm -f $(BINPROGS) $(SBINPROGS) bash_completion zsh_completion

install: all
	install -dm0755 $(DESTDIR)$(PREFIX)/bin
	install -dm0755 $(DESTDIR)$(PREFIX)/sbin
	install -dm0755 $(DESTDIR)$(pkgdatadir)

	install -m0755 ${BINPROGS}   $(DESTDIR)$(PREFIX)/bin
#	install -m0755 ${SBINPROGS}  $(DESTDIR)$(PREFIX)/sbin
	install -m0755 mkarchroot    $(DESTDIR)$(PREFIX)/sbin/archroot

	ln -sf find-libdeps $(DESTDIR)$(PREFIX)/bin/find-libprovides

	install -m0644 lib/common.sh    $(DESTDIR)$(pkgdatadir)/common.sh
	install -m0644 makechrootpkg.in $(DESTDIR)$(pkgdatadir)/makechrootpkg.sh
	install -Dm0644 bash_completion $(DESTDIR)$(PREFIX)/share/bash-completion/completions/devtools
	install -Dm0644 zsh_completion  $(DESTDIR)$(PREFIX)/share/zsh/site-functions/_devtools

uninstall:
	for f in ${BINPROGS}  ; do rm -f $(DESTDIR)$(PREFIX)/bin/$$f; done
#	for f in ${SBINPROGS} ; do rm -f $(DESTDIR)$(PREFIX)/sbin/$$f; done
	rm -f $(DESTDIR)$(PREFIX)/sbin/archroot

	rm -f $(DESTDIR)$(PREFIX)/bin/find-libprovides

	rm -f $(DESTDIR)$(PREFIX)/share/bash-completion/completions/devtools
	rm -f $(DESTDIR)$(PREFIX)/share/zsh/site-functions/_devtools

.PHONY: all clean install uninstall
