#!/bin/bash
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

source @pkgdatadir@/common.sh

CHROOT_VERSION='v2'

FORCE='n'
RUN=''
NOCOPY='n'
NONETWORK='n'

working_dir=''

APPNAME=$(basename "${0}")

# usage: usage <exitvalue>
usage() {
	echo "Usage: ${APPNAME} [options] working-dir [action]"
	echo ' options:'
	echo '    -f            Force overwrite of files in the working-dir'
	echo '    -C <file>     Location of a pacman config file'
	echo '    -M <file>     Location of a makepkg config file'
	echo '    -n            Do not copy config files into the chroot'
	echo '    -c <dir>      Set pacman cache'
	echo '    -N            Disable networking in the chroot'
	echo ' actions:'
	echo '    -i <pkg-list> Install "pkg-list" in the chroot.'
	echo '                  Creates the chroot if necessary, workding-dir must exist'
	echo '    -r <cmd>      Run "cmd" within the context of the chroot'
	echo '    -u            Update the chroot via pacman'
	echo '    -h            Print this message'

	exit ${1-1}
}

################################################################################

while getopts 'fC:M:nc:Nh' arg; do
	case "${arg}" in
		f) FORCE='y' ;;
		C) pac_conf="$OPTARG" ;;
		M) makepkg_conf="$OPTARG" ;;
		n) NOCOPY='y' ;;
		c) cache_dir="$OPTARG" ;;
		N) NONETWORK='y' ;;

		h) action="-$arg" ;;

		*) error "invalid argument '${arg}'"; usage ;;
	esac
done

shift $(($OPTIND - 1))

if (( $# < 2 )); then
	error 'You must specify a directory and an action.'
	usage
fi

working_dir="$(readlink -f "${1}")"
shift 1
[[ -z $working_dir ]] && die 'Please specify a working directory.'

action=$1
shift 1
case "$action" in
	-i) PKGS=("$@") ;;
	-r) RUN="$*" ;;
	-u)
		(( $# > 0 )) && { error 'Extra arguments.'; usage; }
		RUN='/bin/sh -c "pacman -Syu --noconfirm && (pacman -Qqu >/dev/null && pacman -Su --noconfirm || exit 0)"'
		;;
	-h) usage 0 ;;
	-*) error "invalid argument '${action#-}'"; usage ;;
	*) error "invalid action '${action}'"; usage ;;
esac
unset action

################################################################################

if (( $EUID != 0 )); then
	die 'This script must be run as root.'
fi

if [[ -z $cache_dir ]]; then
	cache_dirs=($(pacman -v $cache_conf 2>&1 | grep '^Cache Dirs:' | sed 's/Cache Dirs:\s*//g'))
else
	cache_dirs=(${cache_dir})
fi

host_mirror=$(pacman -Sddp extra/devtools 2>/dev/null | sed -E 's#(.*/)extra/os/.*#\1$repo/os/$arch#')
if echo "${host_mirror}" | grep -q 'file://'; then
	host_mirror_path=$(echo "${host_mirror}" | sed -E 's#file://(/.*)/\$repo/os/\$arch#\1#g')
fi

# {{{ functions
bind_mount() {
	local mode="${2:-rw}"
	local target="${working_dir}${1}"

	if [[ ! -e "$target" ]]; then
		if [[ -d "$1" ]]; then
			install -d "$target"
		else
			install -D /dev/null "$target"
		fi
	fi

	mount -o bind "$1" "$target"
	mount -o remount,${mode},bind "$target"
	mount --make-slave "$target"
}

chroot_mount() {
	trap 'trap_chroot_umount' EXIT INT QUIT TERM HUP

	if (( ! have_nspawn )); then
		bind_mount /sys ro

		[[ -e "${working_dir}/proc" ]] || mkdir "${working_dir}/proc"
		mount -t proc proc -o nosuid,noexec,nodev "${working_dir}/proc"
		bind_mount /proc/sys ro

		[[ -e "${working_dir}/dev" ]] || mkdir "${working_dir}/dev"
		mount -t tmpfs dev "${working_dir}/dev" -o mode=0755,size=10M,nosuid,strictatime
		mknod -m 666 "${working_dir}/dev/null" c 1 3
		mknod -m 666 "${working_dir}/dev/zero" c 1 5
		mknod -m 600 "${working_dir}/dev/console" c 5 1
		mknod -m 644 "${working_dir}/dev/random" c 1 8
		mknod -m 644 "${working_dir}/dev/urandom" c 1 9
		mknod -m 666 "${working_dir}/dev/tty" c 5 0
		mknod -m 666 "${working_dir}/dev/ptmx" c 5 2
		mknod -m 666 "${working_dir}/dev/tty0" c 4 0
		mknod -m 666 "${working_dir}/dev/full" c 1 7
		mknod -m 666 "${working_dir}/dev/rtc0" c 254 0
		ln -s /proc/kcore "${working_dir}/dev/core"
		ln -s /proc/self/fd "${working_dir}/dev/fd"
		ln -s /proc/self/fd/0 "${working_dir}/dev/stdin"
		ln -s /proc/self/fd/1 "${working_dir}/dev/stdout"
		ln -s /proc/self/fd/2 "${working_dir}/dev/stderr"

		[[ -e "${working_dir}/dev/shm" ]] || mkdir "${working_dir}/dev/shm"
		mount -t tmpfs shm "${working_dir}/dev/shm" -o nodev,nosuid,size=128M

		bind_mount /dev/pts

		[[ -e "${working_dir}/run" ]] || mkdir "${working_dir}/run"
		mount -t tmpfs tmpfs "${working_dir}/run" -o mode=0755,nodev,nosuid,strictatime,size=64M

		for host_config in resolv.conf localtime; do
			bind_mount /etc/$host_config ro
		done
	fi

	[[ -n $host_mirror_path ]] && bind_mount "$host_mirror_path" ro

	bind_mount "${cache_dirs[0]}"
	for cache_dir in ${cache_dirs[@]:1}; do
		bind_mount "$cache_dir" ro
	done
}

copy_hostconf () {
	cp -a /etc/pacman.d/gnupg "${working_dir}/etc/pacman.d"
	echo "Server = ${host_mirror}" > ${working_dir}/etc/pacman.d/mirrorlist

	if [[ -n $pac_conf && $NOCOPY = 'n' ]]; then
		cp ${pac_conf} ${working_dir}/etc/pacman.conf
	fi

	if [[ -n $makepkg_conf && $NOCOPY = 'n' ]]; then
		cp ${makepkg_conf} ${working_dir}/etc/makepkg.conf
	fi

	sed -r "s|^#?\\s*CacheDir.+|CacheDir = $(echo -n ${cache_dirs[@]})|g" -i ${working_dir}/etc/pacman.conf
}

trap_unmount_err () {
	error "Error unmounting"
}

trap_chroot_umount () {
	trap 'trap_unmount_err' INT QUIT TERM HUP EXIT

	for cache_dir in ${cache_dirs[@]}; do
		umount "${working_dir}/${cache_dir}"
	done
	[[ -n $host_mirror_path ]] && umount "${working_dir}/${host_mirror_path}"

	if (( ! have_nspawn )); then
		for host_config in resolv.conf localtime; do
			umount "${working_dir}/etc/${host_config}"
		done
		umount "${working_dir}/proc/sys"
		umount "${working_dir}/proc"
		umount "${working_dir}/sys"
		umount "${working_dir}/dev/pts"
		umount "${working_dir}/dev/shm"
		umount "${working_dir}/dev"
		umount "${working_dir}/run"
	fi

	trap 'trap_abort' INT QUIT TERM HUP
	trap 'trap_exit' EXIT
}

chroot_lock () {
	lock_open_write 9 "${working_dir}" "Locking chroot"
}

chroot_run() {
	local dir=$1
	shift
	if (( have_nspawn)); then
		local nspawn_args=(-D "$dir")
		if [[ $NONETWORK = y ]]; then
			nspawn_args+=(--private-network)
		fi
		eval systemd-nspawn "${nspawn_args[@]}" -- "${@}" 2>/dev/null
	else
		local unshare_args=(-mui)
		if [[ $NONETWORK = y ]]; then
			unshare_args+=(-n)
		fi
		eval unshare "${unshare_args[@]}" -- chroot "${dir}" "${@}"
	fi
}

# }}}

# use systemd-nspawn if we have it available and systemd is running
if type -P systemd-nspawn >/dev/null && mountpoint -q /sys/fs/cgroup/systemd; then
	have_nspawn=1
fi

umask 0022
if [[ -n $RUN ]]; then
	# run chroot {{{
	#Sanity check
	if [[ ! -f "${working_dir}/.arch-chroot" ]]; then
		die "'${working_dir}' does not appear to be a Arch chroot."
	elif [[ $(cat "${working_dir}/.arch-chroot") != ${CHROOT_VERSION} ]]; then
		die "'${working_dir}' is not compatible with ${APPNAME} version ${CHROOT_VERSION}. Please rebuild."
	fi

	chroot_lock
	chroot_mount
	copy_hostconf

	chroot_run "${working_dir}" ${RUN}

	# }}}
else
	# {{{ build chroot
	if [[ -e $working_dir && $FORCE = 'n' ]]; then
		die "Working directory '${working_dir}' already exists - try using -f"
	fi

	if { type -P btrfs && btrfs subvolume create "${working_dir}"; } &>/dev/null; then
		chmod 0755 "${working_dir}"
	fi

	chroot_lock
	chroot_mount

	pacargs="${cache_dirs[@]/#/--cachedir=}"
	if [[ -n $pac_conf ]]; then
		pacargs="$pacargs --config=${pac_conf}"
	fi

	if (( $# != 0 )); then
		if [[ $FORCE = 'y' ]]; then
			pacargs="$pacargs --force"
		fi
		if ! pacstrap -GMcd "${working_dir}" ${pacargs} "${PKGS[@]}"; then
			die 'Failed to install all packages'
		fi
	fi

	if [[ -d "${working_dir}/lib/modules" ]]; then
		chroot_run "${working_dir}" ldconfig
	fi

	if [[ -e "${working_dir}/etc/locale.gen" ]]; then
		sed -i 's@^#\(en_US\|de_DE\)\(\.UTF-8\)@\1\2@' "${working_dir}/etc/locale.gen"
		chroot_run "${working_dir}" locale-gen
	fi
	echo 'LANG=C' > "${working_dir}/etc/locale.conf"

	copy_hostconf

	echo "${CHROOT_VERSION}" > "${working_dir}/.arch-chroot"
	# }}}
fi
